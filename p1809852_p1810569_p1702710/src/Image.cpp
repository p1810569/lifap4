#include "Image.h"
#include <iostream>
#include <assert.h>
#include <fstream>


using namespace std;


const int WINSIZE = 500;


Image::Image() {
    
    dimx = 0;
    dimy = 0;
    tab = NULL;
}


Image::~Image() {
    
    afficherDetruit();

    if (tab != NULL)
        delete [] tab;
}


Image::Image(unsigned int dimensionX, unsigned int dimensionY) {

    dimx = dimensionX;
    dimy = dimensionY;
    tab = new Pixel[dimensionX * dimensionY];
}


Pixel& Image::getPix(unsigned int x, unsigned int y) const {
    
    assert(x < dimx && y < dimy);

    return tab[y*dimx + x];
}


void Image::setPix(unsigned int x, unsigned int y, const Pixel& couleur) {

    assert(x < dimx && y < dimy);

    Pixel& pix = getPix(x,y);
    pix.setRouge(couleur.getRouge());    
    pix.setVert(couleur.getVert());    
    pix.setBleu(couleur.getBleu());    
}


void Image::dessinerRectangle(unsigned int Xmin, unsigned int Ymin, 
    unsigned int Xmax, unsigned int Ymax, const Pixel couleur) {

    assert(Xmax < dimx && Ymax < dimy);
    assert(Xmin <= Xmax && Ymin <= Ymax);

    for (unsigned int i=Ymin; i<=Ymax; ++i) {
        for (unsigned int j=Xmin; j<=Xmax; ++j)
            setPix(j, i, couleur);
    }
}


void Image::effacer(const Pixel couleur) {
    
    dessinerRectangle(0, 0, dimx-1, dimy-1, couleur);
}


void Image::testRegression() {

    assert(tab!=NULL && dimx > 10 && dimy > 10 );


    Pixel pix;
    assert(pix.getRouge()==0 && pix.getVert()==0 &&  pix.getBleu()==0);    


    setPix(2, 3, Pixel(255,255,255));
    Pixel& p = getPix(2, 3);
    assert(p.getBleu()==255 && p.getRouge()==255 && p.getVert()==255);

    p = getPix(0,0);
    p.setBleu(1);
    assert(p.getBleu()==1);
    
    dessinerRectangle(5, 5, 10, 10, Pixel(100,100,100));
    
    for (unsigned int i=5; i<10; ++i) {
        for (unsigned int j=5; j<10; ++j) {
            Pixel& p = getPix(i, j);
            assert(p.getRouge()==100 && p.getVert()==100 && p.getBleu()==100);
        }
    }


    effacer(Pixel(0,0,0));
   
    for (unsigned int i=0; i<dimx; ++i) {
        for (unsigned int j=0; j<dimy; ++j) {
            Pixel& p = getPix(i, j);
            assert(p.getRouge()==0 && p.getVert()==0 && p.getBleu()==0);
        }
    }

}

void Image::sauver(const string& filename) const {

    ofstream fichier (filename.c_str());

    assert(fichier.is_open());

    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;

    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {

            Pixel& pix = getPix(x,y);
            fichier << +pix.getRouge() << " " << +pix.getVert() << " " 
                    << +pix.getBleu() << " ";
        }
    }

    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}


void Image::ouvrir(const string& filename) {

    ifstream fichier (filename.c_str());

    assert(fichier.is_open());

	int r,g,b;
	string mot;
	dimx = dimy = 0;

	fichier >> mot >> dimx >> dimy >> mot;

	assert(dimx > 0 && dimy > 0);

	if (tab != NULL) 
        delete [] tab;

	tab = new Pixel [dimx*dimy];

    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {

            fichier >> r >> g >> b;
            
            Pixel& pix = getPix(x,y);

            pix.setRouge(r);
            pix.setVert(g);
            pix.setBleu(b);
        }
    }
    fichier.close();

    cout << "Lecture de l'image " << filename << " ... OK\n";
}


void Image::afficherConsole(){

    cout << dimx << " " << dimy << endl;

    for(unsigned int y=0; y<dimy; ++y) {
        for(unsigned int x=0; x<dimx; ++x) {
            
            Pixel& pix = getPix(x,y);
            cout << +pix.getRouge() << " " << +pix.getVert() << " " 
                 << +pix.getBleu() << " ";
        }
        cout << endl;
    }
}


void Image::afficher() {

    afficherInit();

    SDL_Event events;
	bool quit = false;
    zoom = WINSIZE / max(dimx, dimy) * 0.75;

	while (!quit) {

		while (SDL_PollEvent(&events)) {
			if (events.type == SDL_QUIT)
                quit = true;

            else if (events.type == SDL_KEYDOWN) {

				switch (events.key.keysym.scancode) {

                    case SDL_SCANCODE_T:
                        zoom *= 1.1;
                        break;
                    case SDL_SCANCODE_G:
                        zoom *= 0.9;
                        break;
                    case SDL_SCANCODE_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
				}
			}
        }

        // on clear le buffer avec la couleur gris
        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        SDL_RenderClear(renderer);

		// on rend l'image
		afficherBoucle();

		// on swap le front et le back buffer
        SDL_RenderPresent(renderer);
	}
    cout << "quitter " << quit << endl;

    afficherDetruit();
}


void Image::afficherInit(){

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    // Creation de la fenetre
    window = SDL_CreateWindow("image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINSIZE, WINSIZE, SDL_WINDOW_SHOWN);
    
    if (window == NULL) {
        cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << endl; 
        SDL_Quit(); 
        exit(1);
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    
    if (renderer == NULL) {
		cout << "Erreur lors de la creation du renderer : " << SDL_GetError() << endl; 
        SDL_Quit(); 
        exit(1);
	}


    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, dimx, dimy);
    SDL_PixelFormat* pixel_format = SDL_AllocFormat(SDL_PIXELFORMAT_RGB888);


    Uint32* pixels = nullptr;
    Uint32 format;
    int pitch = 0;

    int w, h;
    SDL_QueryTexture(texture, &format, nullptr, &w, &h);

    if (SDL_LockTexture(texture, nullptr, (void**)&pixels, &pitch))
        cout << "erreur de chargement de la texture : " << SDL_GetError() << endl;


    for (int x = 0; x < w; ++x) {
		for (int y=0; y < h; ++y) {

            Pixel& pix = getPix(x,y);

            Uint32 color = SDL_MapRGB(pixel_format, pix.getRouge(), pix.getVert(), pix.getBleu());
            Uint32 coord = y * (pitch / sizeof(Uint32)) + x;
            pixels[coord] = color;
        }
    }

    SDL_UnlockTexture(texture);
    SDL_FreeFormat(pixel_format);
}

void Image::afficherBoucle(){

    // tille zoome
    int tex_width = dimx*zoom;
    int tex_height = dimy*zoom;

    SDL_Rect rect;
    rect.x = (WINSIZE - tex_width) * 0.5;
    rect.y = (WINSIZE - tex_height) * 0.5;
    rect.w = tex_width;
    rect.h = tex_height;

    SDL_RenderCopy(renderer, texture, NULL, &rect);
}


void Image::afficherDetruit(){

    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
