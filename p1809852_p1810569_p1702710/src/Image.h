#ifndef __IMAGE_H__
#define __IMAGE_H__

#include "Pixel.h"
#include <string>
#include <SDL.h>


class Image {

public :

    /**
     @brief Constructeur par défaut de la classe: initialise une image vide
        ce constructeur n'alloue pas de pixel
     */
    Image();
   

    /** @brief Constructeur de la classe: initialise une image de taille x*y de couleur noir
    */    
    Image(unsigned int dimensionX, unsigned int dimensionY);


    /** @brief Destructeur de la classe: déallocation de la mémoire du tableau de pixel
    */
    ~Image();


    /** 
    @brief Accesseur : récupère le pixel original de coordonnées (x,y) dans l'image
    @param x : coordonné x du pixel
    @param y : coordonné y du pixel
    */
    Pixel& getPix(unsigned int x, unsigned int y) const;


    /**
    @brief Mutateur : modifie la couleur d'un pixel dans l'image au cooordonnés x et y
    @param x : coordonné x du pixel
    @param y : coordonné y du pixel
    @param couleur : couleur du nouveau pixel
    */
    void setPix(unsigned int x, unsigned int y, const Pixel& couleur);


    /** 
    @brief Dessine un rectangle plein dans l'image
    @param Xmin : coordonné du coté gauche du rectangle
    @param Ymin : coordonné du coté bas du rectangle
    @param Xmax : coorfonné du coté droit du rectangle (compris)
    @param Ymax : coordonné du coté haut du rectangle (compris)
    @param couleur : couleur de remplissage du rectangle
    */
    void dessinerRectangle(unsigned int Xmin, unsigned int Ymin, 
        unsigned int Xmax, unsigned int Ymax, const Pixel couleur);


    /** 
    @brief Efface l'image en la remplissant d'une couleur
    @param  couleur : couleur de remplissage  
    */
    void effacer(const Pixel couleur);


    /** 
    @brief Effectue une série de tests vérifiant que le module fonctionne et
    que les données membres de l'objet sont conformes
    @pre L'image doit être au minimum de taille 10 par 10
    */
    void testRegression();


    /** 
    @brief Sauvegarde l'image dans un fichier dont le chemin est passé en paramettre
    @param filename : chemin vers l'image à sauvegarder
    */
    void sauver(const std::string & filename) const;


    /** 
    @brief Charge l'image depuis un fichier
    @param filename : chemin vers l'image à charger
    */
    void ouvrir(const std::string & filename);


    /** 
     @brief  Afficher l'image dans la console
    */
    void afficherConsole();


    /**
     @brief Affiche l'image dans une fenêtre SDL2
    */
    void afficher();

private:
    
    /**
    @brief tableau de pixel
    */ 
    Pixel* tab;

    /**
     @brief les dimensions de l'image
     @param dimx: largeur
     @param dimy: hauteur
    */ 
    unsigned int dimx, dimy;

    void afficherInit();
    void afficherBoucle();
    void afficherDetruit();

    SDL_Window* window;
    SDL_Renderer* renderer;
    SDL_Texture* texture;

    float zoom;
};

#endif // __IMAGE_H__