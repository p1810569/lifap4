#ifndef __PIXEL_H__
#define __PIXEL_H__


class Pixel {

private:
    /** 
        @brief : Les composants du pixel, unsigned int en C++ 
    */
    unsigned char r, g, b;

public:

    /**
        @brief 
        Constructeur par défaut de la classe: initialise le pixel à la couleur noire
    */
    Pixel();
    
    /** @brief Constructeur de la classe : initialise la couleur du pixel
    @param nr : composante rouge
    @param ng : composante verte
    @param nb : composante bleu
    */
    Pixel(unsigned char nr, unsigned char ng, unsigned nb);
   

    /** 
        @brief  
        Accesseur : récupère la composante rouge du pixel
    */
    unsigned char getRouge() const;

    /**
        @brief
        Accesseur : récupère la composante verte du pixel
    */  
    unsigned char  getVert() const;

    /**
        @brief
        Accesseur : récupère la composante bleue du pixel
    */
    unsigned char  getBleu() const;

    /**
        @brief
        Mutateur : modifie la composante rouge du pixel
    */
    void setRouge(unsigned char nr);

    /** 
        @brief 
        Mutateur : modifie la composante verte du pixel
    */
    void setVert(unsigned char nv);

    /** @brief
        Mutateur : modifie la composante bleue du pixel
    */
    void setBleu(unsigned char nb);
};


#endif // __PIXEL_H__