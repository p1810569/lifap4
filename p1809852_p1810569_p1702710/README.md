# Module Image Conception et Développement d'Application

## Membres

    Matthieu JACQUEMET p1810569
    Nicolas LERAY p1809852
    Riyad ENNAOURA p1702710

## Description

Ce module contient plusieurs executables :
- **exemple** : `mainExemple.cpp` génère une image au format `.ppm` est la sauvegarde/charge dans `data/`

- **affichage** : `mainAffichage.cpp` affiche l'image l'un poisson dans une fenettre SDL
- **test** : `mainTest.cpp` effectue le teste de régréssion du module

## Prerequis

    SDL2

## Compilation

Tout génerer:

    make

Générer la documentation 

    make doc

Génerer les executables

    make bin/affichage
    make bin/test
    make bin/exemple

## Utilisation du teste d'affichage

- `T` : pour zoomer.
- `G` : pour dézoomer.
- `ESCAPE` : pour fermer la fenêtre.

## Oragnisation des fichiers/répertoires

- **data/**  : Contient les images 
géneré par les executables
- **obj/**   : Contient les fichier objets de la compilation.

- **doc/**   : Contient la documentation de Image.h et Pixel.h

- **bin/**   : Contient les executables produits.

- **src/**   : Contient les codes source. 

- **Makefile** : Script de compilation.

